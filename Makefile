DK = docker
DKC = docker-compose -f docker-compose.yml
DKCL = ${DKC} -f docker-compose.local.yml

SVC_BE = php
SVC_DB = db
SVC_WEBSERVER = nginx
SVC_FE_WATCHER = npm-watch

DEV_SVCS = ${SVC_BE} ${SVC_FE} ${SVC_DB} ${SVC_WEBSERVER}

JOB_BE_INSTALL = composer-install
JOB_FE_INSTALL = npm-install



info: 
	@echo 'Hello! To start using make, please read the README.md'

up: install
	${DKCL} up -d ${DEV_SVCS} && ${DKCL} ps

stop:
	${DKCL} stop ${DEV_SVCS}

down:
	${DKCL} down 

logs:
	${DKCL} logs

clean: 	down
	rm -rf app/vendor app/node_modules

re-build: down
	${DKCL} build --no-cache

be-install:
	${DKCL} run --rm ${JOB_BE_INSTALL}

fe-install: 
	${DKCL} run --rm ${JOB_FE_INSTALL}

install: be-install fe-install 

watch: fe-install
	${DKCL} run --rm ${SVC_FE_WATCHER}
