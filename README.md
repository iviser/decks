# Decks
The project is a clone of http://portfolio52.com

## How to start developing
### If you use LINUX
First user `make`. There is a number of commands which help you in developing locally.

### Run command in container
Use `./workspace` script to run commands inside `php` container
 
Examples:
 
| Command         | What it does  |
|-----------------|---------------|
| `./workspace php artisan ` | Launch artisan in the container. But there are another convenient way to do it. See `Artisan` paragraph. |
| `./workpsace bash`| Attach to `php` container under current linux user (non-root) |

### Artisan
There is `./artisan` symlink targeted on `.scripts/artisan.sh`. 
Use it to run artisan command within the `php` container. Example:  `./artisan view:clear` .

#### Commands
| Command         | What it does  |
|-----------------|---------------|
| `make up`       | Installs all dependencies and runs the project locally via docker-compose.  |
| `make stop`     | Gracefully stops all the containers (services) |
| `make down`     | Gracefully stops all the docker containers and removes them along with networks, volumes, and images created by `up`. |
| `make watch`    | Runs `npm watch` in container | 
| `make clean`    | Same as `make down` but cleans `app/vendor` and `app/node_modules` additionally |
| `make re-build` | Re-builds all containers images with `--no-cache` option | 
| `make install`  | Runs `composer-isntall` job to install app dependencies ( in `app/vendor`) along with `node isntall` to install node-modules|

### If you use WINDOWS
Use linux instead

### If you use MacOS
Use linux instead

### If you use any other OS
Really?