#!/bin/sh
set -e

# making host record - containerhost

if [ ! -f "/var/www/html/.macosx" ]; then
    route | grep 'default' | cut -d: -f2 | awk '{print $2"  containerhost"}' >> /etc/hosts;
else
    echo "192.168.0.157    containerhost" >> /etc/hosts;
fi

#sleep 3000

# default behaviour is to launch nginx
echo "Starting nginx..."
exec $(which nginx) -c /etc/nginx/nginx.conf -g "daemon off;" ${EXTRA_ARGS}
