#!/bin/sh
set -e

# making host record - containerhost
route | grep 'default' | cut -d: -f2 | awk '{print $2"  containerhost"}' >> /etc/hosts;

# chown files as user if we on linux

MACOS_FILEPATH=/var/www/html/.macosx
INDEX_PHP_FILEPATH=/var/www/html/public/index.php

if [ ! -f "$MACOS_FILEPATH" ] && [ -f "$INDEX_PHP_FILEPATH" ]; then
    mark_file="$INDEX_PHP_FILEPATH"

    uid=$(stat -c %u ${mark_file})
    gid=$(stat -c %g ${mark_file})

    usermod -u "${uid}" www-data
    groupmod -g "${gid}" www-data

    echo "AUTO CHOWN www-data for /var/www/html"
    chown -R www-data:www-data /var/www/html
fi

# if ENV debug - let xdebug live
if [ "$XDEBUG" = "1" ]; then
  echo "xdebug-enabled"
  # if ENV debug-always-on - let xdebug live always
  if [ "$XDEBUG_ALWAYS_ON" = "1" ]; then
    echo "xdebug-always-on"
    sed -i 's/xdebug.remote_autostart=off/xdebug.remote_autostart=on/g' /usr/local/etc/php/conf.d/30-docker-php-ext-xdebug.ini
  fi

  # if ENV debug-always-on - let xdebug live always
  if [ "$XDEBUG_PROFILER_ON" = "1" ]; then
    echo "xdebug-profiler-on"
    sed -i 's/xdebug.profiler_enable=off/xdebug.profiler_enable=on/g' /usr/local/etc/php/conf.d/30-docker-php-ext-xdebug.ini
  else
    rm -rf /var/www/html/.profiling/*
  fi
else
  rm -rf /usr/local/etc/php/conf.d/30-docker-php-ext-xdebug.ini
fi

# if ENV tideways - let tideways mode ON
if [ "$TIDEWAYS_XHPROF" = "1" ]; then
    echo "tideways-xhprof-enabled"
else
  rm -rf /usr/local/etc/php/conf.d/50-tideways-xhprof-7.2.ini
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
  # see https://unix.stackexchange.com/questions/308260/what-does-set-do-in-this-dockerfile-entrypoint
	set -- php-fpm "$@"
fi

exec "$@"
