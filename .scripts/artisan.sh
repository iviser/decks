#!/bin/sh

main ()  {
  local UID=${UID:-1000}
    	
	local DK='docker'
	local DKC='docker-compose -f docker-compose.yml'
	local DKCL="${DKC} -f docker-compose.local.yml"
		
	local SVC_BE='php'
	${DKCL} exec --user=${UID} php php artisan $@


}

main $@

