@extends('layouts.app')
@section('content')
<div class="row">
        <form method="post" action="{{route('user.add')}}">
            @csrf
            <label>Имя:</label>
            <input class="form-control" type="text" name="name">
            <label>Email:</label>
            <input class="form-control" type="email" name="email">
            <label>Пороль:</label>
            <input class="form-control" type="password" name="password">
            <button class="btn btn-success" type="submit">Создать</button>
        </form>
    </div>
@endsection
