@extends('layouts.app')
@section('content')
    <style>

        .box {
            display: flex;
            flex-wrap: wrap;
        }

        .img-1 {
            position: absolute;
        }

        .text {
           text-align: center;
        }

        .img-1:hover {
            opacity: 0;
        }
    </style>
    @if(\Illuminate\Support\Facades\Auth::check())
    <div class="container-2">
        <form action="{{route('decks.create')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input name="name" type="text" class="form-control" required placeholder="Название колоды"><br>
            <input name="description" type="text" class="form-control" required placeholder="Описание колоды"><br>
            <label>Загрузка картинки передняя часть:</label>
            <input name="img" type="file">
            <label>Загрузка картинки задняя часть:</label>
            <input name="img_back" type="file">
            <button type="submit" class="btn btn-primary">Отправить</button>
        </form>
    </div>
@else
        <div style="display: flex;justify-content: center" class=""><h1>Авторизуйтесь что-бы загрузить карты.</h1></div>
@endif
    @foreach($decks as $deck)

        <div class="box">
            <div style="width: 70%" class="text">
                <h1>{{ $deck->name }}</h1>
                <h2>{{ $deck->description }}</h2>
                <h3>{{ $deck->user_id }}</h3>

            </div>
            <div style="margin-left: auto;margin-right: auto;" class="img-box">
                <img  class="img-1" src="{{asset('/storage/'.$deck->img)}}" width="205px" height="300px">
                <img  class="img-2" src="{{asset('/storage/'.$deck->img_back)}}" width="205px" height="300px">
            </div>
        </div>
        <br>

    @endforeach

@endsection


