@extends('layouts.app')
@section('content')
<style>
    .index {
        display: flex;
        flex-wrap: wrap;
        width: 100%;
    }

    .box {
        width: 30%;
        height: 300px;
        border: 2px black solid;
        margin: 10px 0 0 10px;
    }

    .box h1, .box h2 {
        text-align: center;
    }

    @media screen and  (min-width: 500px)  and (max-width: 1200px) {
        .index {

        }
        .box h1 {
            font-size: 20px;
        }

        .box h2 {
            font-size: 15px;
        }

        .box {
            width: 23%;
            height: 200px;
        }

    }


    @media screen and  (min-width: 300px)  and (max-width: 500px) {
        .box {
            width: 45%;
            height: 200px;
        }
        .box h1{
            font-size: 15px;
        }
        .box h2{
            font-size: 15px;
        }
    }
</style>
<div class="index">
    <div style="width: 100%; display: flex;justify-content: center;margin-top: 20px;" class="">
        <a style="text-decoration: none;color: white" href="{{route('user.create')}}"> <button style="font-size: 20px;;width: 200px;height: 100px" class="btn btn-primary">Создать запись</button></a>
    </div>
        @foreach($users as $user)
        <div class="box">
            <h1>Имя</h1>
            <h2>{{$user->name}}</h2>
            <hr>
            <h1>Имеил</h1>
            <h2>{{$user->email}}</h2>
            <button class="btn btn-warning" type="submit"><a style="color: black" href="/user/{{$user->id}}/edit">Редактировать</a></button>
            <button class="btn btn-danger" type="submit"><a style="color: #eee4e4" href="/user/{{$user->id}}/delete">Удалить</a></button>
        </div>

    @endforeach
</div>
@endsection
