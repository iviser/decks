@extends('layouts.app')
@section('content')
<style>
    body {
        background: #eee4e4;
    }
    .container {
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        justify-items: center;
    }
    .user-content {
        margin-top: 20%;
        background: #eee4e4;
        width: 50%;
        height: 23%;
        border: #bdd8e5 10px solid;
        border-radius: 10px;
    }
</style>

<div class="container">
<div class="user-content">
    <form method="post" action="/user/{{$user->id}}/update">
        @csrf
        <label>Имя:</label>
        <input type="text" class="form-control" name="name" value="{{$user->name}}" required>
        <label>Email:</label>
        <input type="email" class="form-control" name="email" value="{{$user->email}}" required>
        <button type="submit" style="margin-top: 10px" class="btn btn-warning">Обновить</button>
    </form>
</div>
</div>
@endsection
