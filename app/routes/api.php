<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\DecksController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'admin'], function () {

    // @TODO Apply REST approach
    Route::group(['prefix' => 'users'], static function () {
        Route::get('/',[UserController::class, 'index']);

        Route::get('/{id}/edit', [UserController::class, 'edit']);

        Route::post('/{id}/update', [UserController::class, 'update']);

        Route::post('/{id}/delete', [UserController::class, 'delete']);

        Route::get('/create', [UserController::class, 'create']);

        Route::post('/add', [UserController::class, 'userAdd']);
    });

    Route::group(['prefix' => 'company'], static function () {
        Route::post('/', [CompanyController::class, 'store']);
        Route::post('/{companyId}', [CompanyController::class, 'update']);
    });


    Route::group(['prefix' => 'decks'], function () {
        Route::get('/', [DecksController::class, 'index'])->name('decks.index');
    });
});


