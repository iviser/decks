<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * TODO get rid off views
 */
class UserController extends Controller
{
    public function index(): JsonResponse
    {
        $data = User::query()->get();
        return response()->json(['data' => $data]);
    }

    public function edit($id): View
    {
        $user = User::query()->find($id);
        return view('edit', ['user' => $user]);
    }

    public function update(Request $request, $id)
    {
        /** @var User $user */
        $user = User::query()->find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();
        return redirect(route('index'));
    }

    /**
     * @param $id
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function delete($id): RedirectResponse
    {
        User::query()->find($id)->delete();
        return redirect(route('index'));
    }

    public function create(): View
    {
        return view('create');
    }

    public function userAdd(Request $request): JsonResponse
    {
        $user = User::query()->create($request->input());

        return response()->json($user, 201);
    }
}
