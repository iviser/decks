<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class CompanyController extends Controller
{
    private ResponseFactory $responseFactory;
    private LoggerInterface $logger;

    public function __construct(ResponseFactory $responseFactory, LoggerInterface $logger)
    {
        $this->responseFactory = $responseFactory;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $rules = [
            'deck_id' => 'required|exits:decks,id',
            'name'    => 'required|string'
        ];

        $this->validate($request, $rules);

        try {
            DB::transaction(static function () use ($request) {
                Company::query()->create([
                    'deck_id' => $request->get('deck_id'),
                    'name' => $request->get('name')
                ]);
            });

            DB::commit();

            return $this->responseFactory->json([], Response::HTTP_CREATED);
        } catch (Exception $exception) {
            DB::rollBack();

            $this->logger->error($exception->getMessage(), [
                'exception' => $exception
            ]);

            return $this->responseFactory->json(
                ['error' => 'Could not create company'],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }
    }

    /**
     * @param Request $request
     * @param string $id
     * @throws ValidationException
     */
    public function update(Request $request, string $id): void
    {
        $roles = [
            'name' => 'required|string'
        ];
        $this->validate($request, $roles);
        try {
            DB::transaction(function () use ($request, $id) {
                Company::query()->findOrFail($id)->update([
                   'deck_id' => $request->get('deck_id'),
                   'name' => $request->get('name')
                ]);
            });
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
