<?php

namespace App\Http\Controllers;

use App\Models\Decks;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DecksController extends Controller
{
    public function index(): JsonResponse
    {
        $data = Decks::all();
        return response()->json(['data' => $data]);
    }

    public function getDecks(): JsonResponse
    {
        $decks = Decks::all();
        return response()->json(['decks' => $decks]);
    }

    public function create(Request $request)
    {
        Decks::query()->create(
            [
                'name'        => $request->input('name'),
                'description' => $request->input('description'),
            ]
        );
        return redirect(route('decks.main'));
    }
}
