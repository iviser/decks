<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 * @package App\Models
 * @property string $id
 * @property string $name
 */
class Company extends Model
{
    use UsesUuid;

    protected $fillable= [
        'deck_id',
        'name',
    ];
}
