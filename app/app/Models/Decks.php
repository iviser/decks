<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Decks
 * @package App\Models
 * @property string $id
 * @property string $deckId
 * @property string $name
 * @property string $description
 */
class Decks extends Model
{
    use UsesUuid;

    protected $table = 'decks';
    protected $fillable = [
        'name',
        'description'
    ];

    public function company(): HasOne
    {
        return $this->hasOne(Company::class);
    }
}

