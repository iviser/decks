<?php


namespace App\Models;


use Illuminate\Support\Str;

trait UsesUuid
{
    protected static function boot(): void
    {
        /** @noinspection PhpUndefinedClassInspection */
        parent::boot();
        static::creating(static function (self $model) {
            if ($model->keyAttributeIsSet()) {
                return;
            }
            $model->setAttribute($model->getKeyName(), Str::orderedUuid()->toString());
        });
    }
    /** @noinspection PhpUnused */
    public function getKeyType(): string
    {
        return 'string';
    }
    public function getIncrementing(): bool
    {
        return false;
    }
    public function keyAttributeIsSet(): bool
    {
        $hasKeyAttribute = array_key_exists($this->getKeyName(), $this->attributes);
        return $hasKeyAttribute && $this->getKey() !== null;
    }

}
