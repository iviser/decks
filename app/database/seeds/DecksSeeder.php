<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use App\Models\Decks;

class DecksSeeder extends Seeder
{

    public function run(): void
    {
        Decks::query()->truncate();

        $faker = Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Decks::query()->create(
                [
                    'name' => $faker->text,
                    'description' => $faker->text
                ]
            );
        }
    }
}
