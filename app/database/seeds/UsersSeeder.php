<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use App\User;
use \Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    public function run(): void
    {
        User::query()->truncate();

        $faker = Factory::create();

        $password = Hash::make($faker->streetName);

        for ($i=0; $i < 30; $i++)
        {
            User::query()->create([
                'name'=> $faker->name,
                    'email' => $faker->email,
                    'password' => $password
                ]
            );
        }
    }
}
